import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent {

  // i18nSelect
  nombre: string = 'Fernando';
  genero: string = 'masculino';

  invitacionMapa = {
    'masculino' : 'invitarlo',
    'femenino' : 'invitarla'
  }

  // i18nPlural
  clientes: string[] = ['María','Pedro','Hernando','Eduardo','Fernando'];
  clientesMapa = {
    '=0':'no tenemos ningún cliente esperando.',
    '=1':'tenemos un cliente esperando.',
    '=2':'tenemos 2 clientes esperando.',
    'other':'tenemos # clientes esperando.'
  }

  cambiarcliente(){
    this.nombre = "María";
    this.genero = "femenino"
  }

  borrarcliente(){
    this.clientes.pop();
  }

  // KeyValue Pipe
  persona = {
    nombre: 'fernando',
    edad: 35,
    direccion: 'Ottawa, Canadá'
  }

  // JsonPipe

  heroes = [
    {
      nombre: 'Superman',
      vuela: true
    },
    {
      nombre: 'Robin',
      vuela: false
    },
    {
      nombre: 'Aquaman',
      vuela: false
    }
  ];

  // Async Pipe

  miObservable = interval(5000).pipe(tap(  () => console.log('interval') ));//0,1,2,3,4,..

  valorPromesa = new Promise( (resolve, reject) => {
    setTimeout(() =>{
      resolve('Tenemos data de la promesa')
    },3500);
  });
}
